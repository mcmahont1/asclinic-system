.. _workflows:

WORKFLOWS
=========

.. toctree::
  :maxdepth: 2
  :hidden:

  workflow_git_merge_from_upstream_fork
  workflow_gpio_single_pin
  workflow_i2c
  workflow_usb_camera_settings
  workflow_camera_calibration
  workflow_aruco_detection



The following workflows are intended to assist you to get familiar with the various parts of the software for the robot:

* Workflow for: :ref:`Git merging changes from an upstream fork into your repository <workflow-git-merge-from-upstream-fork>`.
* Workflow for: :ref:`ROS interface with a single GPIO pin <workflow-gpio-single-pin>`.
* Workflow for: :ref:`ROS interface with an I2C bus <workflow-i2c>`.
* Workflow for: :ref:`Adjusting the controls of a USB camera <workflow_usb_camera_settings>`.
* Workflow for: :ref:`Recording images from a camera, and subsequently using these for calibrating the intrinsic parameters <workflow_camera_calibration>`.
* Workflow for: :ref:`Dececting aruco markers in camera images, and estimating the pose of the marker relative to the camera <workflow_aruco_detection>`.
