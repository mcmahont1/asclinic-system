.. _ros-why:

Why use ROS (for robotics)?
===========================

The sensible starting point to answer to this is given on the `ROS wiki page <https://wiki.ros.org/ROS/Introduction#Goals>`_ where it states the following: