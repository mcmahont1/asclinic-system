#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from asclinic_pkg.msg import TrajectoryChunk, GenTrajectoryCommand, Pose

class SensorFusion():
    def init(self):
        self.pose = Pose()
        self.pose.x=0
        self.pose.y=0
        self.odomSubscriber = rospy.Subscriber("odometry/pose_estimate", Pose, self.odomCallback)

    def odomCallback(self, msg):
        self.pose = msg


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "SensorFusion"
    rospy.init_node(node_name)
    template_py_node = SensorFusion()
    # Spin as a single-threaded node
    rospy.spin()
