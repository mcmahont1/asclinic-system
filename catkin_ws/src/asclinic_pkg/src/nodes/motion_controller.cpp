// Include some useful libraries
#include <math.h>
#include <stdlib.h>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include <std_msgs/String.h>

// Include the asclinic message types
#include "asclinic_pkg/ErrorMessage.h"
#include "asclinic_pkg/ThetaDotMessage.h"
#include "asclinic_pkg/Pose.h"
#include "asclinic_pkg/TrajectoryChunk.h"

// Namespacing the package
using namespace asclinic_pkg;

#define EPSILON 0.00001f
#define WHEEL_RAD 0.072f
#define WHEEL_BASE 0.109f
#define TAU 0.02f
#define T 0.02f
#define UPDATE_PERIOD 0.1f

float prev_err_v = 0.0;
float prev_err_omega = 0.0;
float v_error = 0.0;
float omega_error = 0.0;
float differentiator_v = 0.0; 
float differentiator_omega = 0.0;

float desired_pose[4];
float current_pose[3];// = {1,2,0};

// -----------------------------------------------------------
// V   V    A    RRRR   III    A    BBBB   L      EEEEE   SSSS
// V   V   A A   R   R   I    A A   B   B  L      E      S    
// V   V  A   A  RRRR    I   A   A  BBBB   L      EEE     SSS 
//  V V   AAAAA  R  R    I   AAAAA  B   B  L      E          S
//   V    A   A  R   R  III  A   A  BBBB   LLLLL  EEEEE  SSSS 
// -----------------------------------------------------------

// Publisher variable:
ros::Publisher m_template_publisher;
ros::Publisher m_error_message_publisher;
ros::Publisher m_theta_dot_publisher;


// Timer variable used for recurrent publishing:
ros::Timer m_timer_for_publishing;


void error_function(){
	float error_v = 0.0, error_omega = 0.0;
	bool isRotational = true, isIdle = false;
	float ang_thresh = 3.1415926535f*0.1;
	float x_dist = desired_pose[1] - current_pose[0];
	float y_dist = desired_pose[2] - current_pose[1];
	float delta_phi = desired_pose[3] - current_pose[2];
	float dist = sqrt((x_dist*x_dist)+(y_dist*y_dist));

	if(delta_phi < ang_thresh){
		isRotational = false;
	}

	if((dist < EPSILON) && (delta_phi < EPSILON)){
		isIdle = true;
	}

	if(!isRotational && !isIdle){
		ROS_INFO_STREAM("[MOTION CONTROLLER] NORMAL");
		float gamma = atan2(y_dist, x_dist);

		error_v = dist*cos(gamma - desired_pose[3]);
		error_omega = dist*sin(gamma - desired_pose[3]);
		
	}else if (isRotational && !isIdle){
		
		ROS_INFO_STREAM("[MOTION CONTROLLER] ROTATIONAL");
		error_v = 0.0;
		error_omega = delta_phi;
	}

	// Publish message
	ErrorMessage error = ErrorMessage();

	error.v = error_v;
	error.omega = error_omega*0.1;
	ROS_INFO_STREAM("[MOTION CONTROLLER] ("<<error_v<<", "<<error_omega<<")");
	m_error_message_publisher.publish(error);
	
}



// > Callback run when timer fires
void timerCallbackForPublishing(const ros::TimerEvent&);

// > Callback run when subscriber receives a message
void errorSubscriberCallback(const ErrorMessage& error);


// > Callback run when time fires
void timerCallbackForPublishing(const ros::TimerEvent&){
	float kp_v  = 2.0; 
	float kp_omega = 6.0;
	float kd_omega = 2;

	// Declare a static integer for publishing a different
	// number each time
	static uint counter = 0;

	// Increment the counter
	counter++;

	error_function();
	
	float v = kp_v*v_error;

	differentiator_omega = 	0; //-1.0*(2.0*kp_omega* (omega_error - prev_err_omega)) + ((2.0*TAU - T)* differentiator_omega)/(2.0*TAU + T);
	float omega = (kp_omega*omega_error) + differentiator_omega;

	float theta_dot_l = v/WHEEL_RAD - (WHEEL_BASE*omega)/(WHEEL_RAD);
	float theta_dot_r = v/WHEEL_RAD + (WHEEL_BASE*omega)/(WHEEL_RAD);

	// Initialise a "TemplateMessage" struct
	// > Note that all value are set to zero by default
	ThetaDotMessage theta_dot_message = ThetaDotMessage();
	// Set the values
	theta_dot_message.left = theta_dot_l;
	theta_dot_message.right = theta_dot_r;

	ROS_INFO_STREAM("[MOTION CONTROLLER] Theta_dot: (" << theta_dot_l << ","<< theta_dot_r<<")");

	// Publish the message
	m_theta_dot_publisher.publish(theta_dot_message);

	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	m_timer_for_publishing.stop();
	// > Set the period again (second argument is reset)
	m_timer_for_publishing.setPeriod( ros::Duration(UPDATE_PERIOD), true);

	// > Start the timer again
	m_timer_for_publishing.start();
}

// > Callback run when subscriber receives a message
void errorSubscriberCallback(const ErrorMessage& error){
	// Display that a message was received
	ROS_INFO_STREAM("[MOTION CONTROLLER NODE] Message receieved with error (" << error.v << ","<< error.omega<<")");
	prev_err_v = v_error;
	prev_err_omega = omega_error;

	v_error = error.v;
	omega_error = error.omega;

	differentiator_v += v_error;
	differentiator_omega += omega_error;

}

void DesiredPoseSubscriberCallback(const TrajectoryChunk& pose){
	// ROS_INFO_STREAM("[MOTION CONTROLLER NODE] Message receieved with desired pos (" << pose.x << ","<< pose.y<< ","<< pose.phi<<")"<< std::endl);
	desired_pose[0] = pose.timestamp;
	desired_pose[1] = pose.x;
	desired_pose[2] = pose.y;
	desired_pose[3] = pose.phi;
	ROS_INFO_STREAM("[MOTION CONTROLLER] DESIRED POSE RETRIEVED HIUHIUASUHDIUASHIUDHASIUDHAISUDHIASUHDIAUSUHDIUASHDI");
}

void CurrentPoseSubscriberCallback(const Pose& pose){
	// ROS_INFO_STREAM("[MOTION CONTROLLER NODE] Message receieved with current pose (" << pose.x << ","<< pose.y<< ","<< pose.phi<<")"<< std::endl);
	current_pose[0] = pose.x;
	current_pose[1] = pose.y;
	current_pose[2] = pose.phi;
	ROS_INFO_STREAM("[MOTION CONTROLLER] POSE ESTMATE RETRIEVED IHDSIAHSUDIUSAHDIUASHDIUSHDIUASHIDAHSIUDHASIUDHDIUAH");
}



//    ------------------------
//    M   M    A    III  N   N
//    MM MM   A A    I   NN  N
//    M M M  A   A   I   N N N
//    M   M  AAAAA   I   N  NN
//    M   M  A   A  III  N   N
//    ------------------------

int main(int argc, char* argv[]){

	// STARTING THE ROS NODE

	ros::init(argc, argv, "motion_controller");

	// CONSTRUCT A NODE HANDLE
	ros::NodeHandle nodeHandle("/");

	// GET THE NAMESPACE OF THIS NODE
	// > This can be useful for debugging because you may be
	//   launching the same node under different namespaces.
	std::string m_namespace = ros::this_node::getNamespace();
	// Display the namespace
	// ROS_INFO_STREAM("[MOTION CONTROLLER	CPP NODE] ros::this_node::getNamespace() =  " << m_namespace);

	// INITIALISE THE PUBLISHER
	//   ros::Publisher advertise<<message_type>>(const std::string& topic, uint32_t queue_size, bool latch = false);
	m_error_message_publisher = nodeHandle.advertise<ErrorMessage>(m_namespace+"/error_topic", 10, false);
	m_theta_dot_publisher = nodeHandle.advertise<ThetaDotMessage>(m_namespace+"/theta_dot_topic", 10, false);

	// INITIALISE THE TIMER FOR RECURRENT PUBLISHING
	// Initialise the member variable "m_timer_for_publishing" to be
	// a "ros::Timer" type variable. The syntax for the call to
	// initialise the timer is:
	//   ros::Timer ros::NodeHandle::createTimer(ros::Duration period, <callback>, bool oneshot = false);
	// where:
	//   period
	//     This is the period between calls to the timer callback. For
	//     example, if this is ros::Duration(0.1), the callback will be
	//     scheduled for every 1/10th of a second.
	//   <callback>
	//     This is the callback to be called -- it may be a function,
	//     class method or functor object.
	//   oneshot
	//     Specifies whether or not the timer is a one-shot timer.
	//     If so, it will only fire once. Otherwise it will be re-scheduled
	//     continuously until it is stopped.
	//     If a one-shot timer has fired it can be reused by calling stop()
	//     along with setPeriod(ros::Duration) and start() to be re-scheduled
	//     once again.
	m_timer_for_publishing = nodeHandle.createTimer(ros::Duration(UPDATE_PERIOD), timerCallbackForPublishing, true);



	// INITIALISE THE SUBSCRIBER
	// Initialise a local variable "template_subscriber" to be a
	// "ros::Subscriber" type variable that subscribes to the
	// topic named "template_topic". The subscriber can be a local
	// variable because as long as the main function is running,
	// i.e., as long as this node is active, then the subsriber will
	// trigger the callback function when messages are received. The
	// syntax for the call to initialise the subscriber is:
	//   .subscribe((const std::string& topic, uint32_t queue_size, <callback>, const ros::TransportHints& transport_hints = ros::TransportHints());
	// where:
	//   topic
	//     String that defines the name of which topic to subscribe to
	//   queue_size
	//     Integer that defines the number of messages buffered.
	//   <callback>
	//     This is the callback to be called. Depending on the version
	//     of subscribe() you're using, this may be any of a few things.
	//     The most common is a class method pointer and a pointer to
	//     the instance of the class.
	//   transport_hints
	//     The transport hints allow you to specify hints to roscpp's
	//     transport layer. This lets you specify things like preferring
	//     a UDP transport, using tcp nodelay, etc.
	//     
	//
	/**
	 * The subscribe() call is how you tell ROS that you want to receive messages
	 * on a given topic.  This invokes a call to the ROS
	 * master node, which keeps a registry of who is publishing and who
	 * is subscribing.  Messages are passed to a callback function, here
	 * called chatterCallback.  subscribe() returns a Subscriber object that you
	 * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
	 * object go out of scope, this callback will automatically be unsubscribed from
	 * this topic.
	 *
	 * The second parameter to the subscribe() function is the size of the message
	 * queue.  If messages are arriving faster than they are being processed, this
	 * is the number of messages that will be buffered up before beginning to throw
	 * away the oldest ones.
	 */
	ros::Subscriber error_subscriber = nodeHandle.subscribe(m_namespace+"/error_topic", 1, errorSubscriberCallback);
	ros::Subscriber desired_pose_subsrciber = nodeHandle.subscribe(m_namespace+"/trajectoryGen/pose_reference", 1, DesiredPoseSubscriberCallback);
	ros::Subscriber current_pose_sbscriber = nodeHandle.subscribe("/pose_estimate", 1, CurrentPoseSubscriberCallback);


	// Display that the node is ready
	ROS_INFO("[MOTION CONTROLLER CPP NODE] Ready :-)");

	// SPIN AS A SINGLE-THREADED NODE
	ros::spin();

	return 0;
}
