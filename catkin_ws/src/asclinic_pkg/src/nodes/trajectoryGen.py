#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from std_msgs.msg import Float32MultiArray, MultiArrayDimension
from Queue import PriorityQueue
from std_srvs.srv import Trigger, TriggerResponse
from asclinic_pkg.msg import TrajectoryChunk, GenTrajectoryCommand, Pose
from constant import X_INDEX, Y_INDEX, PHI_INDEX

class Graph:
    def __init__(self, num_of_vertices):
        self.v = num_of_vertices
        self.edges = [[-1 for i in range(num_of_vertices)] for j in range(num_of_vertices)]
        self.visited = []
    def add_edge(self, u, v, weight):
        self.edges[u][v] = weight
        self.edges[v][u] = weight
    def dijkstra(self, start_vertex, end_vertex):
        D = {v: float('inf') for v in range(self.v)}
        prev = [-1 for i in range(self.v)]
        D[start_vertex] = 0

        pq = PriorityQueue()
        pq.put((0, start_vertex))

        while not pq.empty():
            (dist, current_vertex) = pq.get()
            self.visited.append(current_vertex)

            for neighbor in range(self.v):
                if self.edges[current_vertex][neighbor] != -1:
                    distance = self.edges[current_vertex][neighbor]
                    if neighbor not in self.visited:
                        old_cost = D[neighbor]
                        new_cost = D[current_vertex] + distance
                        if new_cost < old_cost:
                            pq.put((new_cost, neighbor))
                            D[neighbor] = new_cost
                            prev[neighbor] = current_vertex
        path = [end_vertex]
        while path[0] != start_vertex:
            path.insert(0, prev[path[0]])
        return path


def norm(vect):
    return np.sqrt(sum(i ** 2 for i in vect))


def gradC(p1, p2):
    if p1[0] == p2[0]:
        m = np.Inf
        c = p1[0]
    else:
        m = (p2[1] - p1[1]) / (p2[0] - p1[0])
        c = p1[1] - m * p1[0]
    return [m, c]


def isCollide(p1, p2, p3, p4):
    inf = np.Inf
    [m1, c1] = gradC(p1, p2)
    [m2, c2] = gradC(p3, p4)
    res = 0
    if m1 == inf:
        if m2 == inf:
            if c1 == c2:
                res = 1
        else:
            y = m2 * c1 + c2
            if ((y > p1[1]) and (y < p2[1])) or ((y < p1[1]) and (y > p2[1])):
                if ((y > p3[1]) and (y < p4[1])) or ((y < p3[1]) and (y > p4[1])):
                    res = 1
    if m2 == inf:
        y = m1 * c2 + c1
        if ((y > p1[1]) and (y < p2[1])) or ((y < p1[1]) and (y > p2[1])):
            if ((y > p3[1]) and (y < p4[1])) or ((y < p3[1]) and (y > p4[1])):
                res = 1
    if m1 != m2:
        x = (c2 - c1) / (m1 - m2)
        if ((x > p1[0]) and (x < p2[0])) or ((x < p1[0]) and (x > p2[0])):
            if ((x > p3[1]) and (x < p4[0])) or ((x < p3[0]) and (x > p4[0])):
                res = 1
    else:
        if c1 == c2:
            res = 1
    return res


def checkClockwise(p1, p2, p3):
    tot = 0
    tot = tot + (p2[0] - p1[0]) * (p2[1] + p1[1])
    tot = tot + (p3[0] - p2[0]) * (p3[1] + p2[1])
    tot = tot + (p1[0] - p3[0]) * (p1[1] + p3[1])
    return tot > 0


def doCollideCheck(p_1, p_2, N, spec):
    flag = 0
    for index in range(N - 1):
        p_3 = [spec[index][0], spec[index][1]]
        p_4 = [spec[index + 1][0], spec[index + 1][1]]
        if isCollide(p_1, p_2, p_3, p_4) == 1:
            # disp("CollideStartEnd")
            flag = 1
            break
    return flag


def get_buffer(p, p_back, p_forward, dist):
    v1 = np.subtract(p, p_back)
    if(norm(v1)==0):
        rospy.loginfo("divide by zero on points" + str(p) + str(p_back) + str(p_forward))
    v1 = v1 / norm(v1)
    v2 = np.subtract(p, p_forward)
    if(norm(v2)==0):
        rospy.loginfo("divide by zero on points" + str(p) + str(p_back) + str(p_forward))
    v2 = v2 / norm(v2)
    vect = v1 + v2
    if np.all(vect == [0, 0]):
        res = [-v1[1], v1[0]]
    else:
        res = vect / norm(vect)
    res = res * dist
    if ~checkClockwise(p, p_back, p + res) and ~checkClockwise(p_forward, p, p + res):
        res = -res
    return res


def checkPointBufferCollide(p1, p2, N, spec, buffer):
    res = False
    # rospy.loginfo("cPBC function points "+ str(p2) + str(p1))
    for q in range(N - 1):
        p0 = spec[q]
        l = abs((p2[0] - p1[0]) * (p1[1] - p0[1]) - (p1[0] - p0[0]) * (p2[1] - p1[1]))
        n = norm(np.subtract(p2, p1))
        l = l / norm(np.subtract(p2, p1))
        # disp([q l])
        e1 = np.subtract(p2, p1)
        e2 = np.subtract(p0, p1)
        recArea = np.dot(e1, e1)
        val = np.dot(e1, e2)
        if (norm(np.subtract(p1, p0)) < buffer) or (norm(np.subtract(p2, p0)) < buffer):
            res = True
            # disp("LINEPOINTCOLLIDE")
            break
        if (l < buffer) and (val >= 0) and (val <= recArea):
            res = True
            # disp("LINEPOINTCOLLIDE")
            break
    return res


def genTrajectoryDescription(planned_path):
    trajectory_description = []
    factor = 1
    resolution = 0.1 * factor
    timestep = 1.0 * factor
    time = 2
    L = len(planned_path)
    rospy.loginfo("planned_path is " + str(planned_path))
    for i in range(L - 1):
        length = norm(np.subtract(planned_path[i], planned_path[i + 1]))
        angle = np.arctan2(planned_path[i + 1][1] - planned_path[i][1], planned_path[i + 1][0] - planned_path[i][0])
        for k in range(int(np.floor(length / resolution) - 1)):
            time = time + timestep
            trajectory_description.append([time, planned_path[i][0] + np.cos(angle) * k * resolution, planned_path[i][1] + np.sin(angle) * k * resolution, angle])

        time = time + timestep
        if i<L-2:
	    angle = np.arctan2(planned_path[i + 2][1] - planned_path[i + 1][1],
                                                        planned_path[i + 2][0] - planned_path[i + 1][0])
        trajectory_description.append([time, planned_path[i + 1][0], planned_path[i + 1][1], angle])
    return trajectory_description


class trajectoryGen:

    def __init__(self):
        # Initialise a publisher
        self.template_publisher = rospy.Publisher(node_name + "/pose_reference", TrajectoryChunk, queue_size=10)
        # Initialise pose
        self.pose = [0.1, 0.1, 0]
        self.start_time = rospy.get_time()
        self.warehouse_specification = [[0.0, 0.0], [2.0, 0.0], [3.0, 1.0], [3.0, 2.0], [2.0, 2.0],[2.0, 2.5], [3.0, 3.5], [3.0, 4.5], [0.0, 4.5], [0.0, 0.0]] # Fix this
        self.trajectory_description = genTrajectoryDescription(self.getPath([1, 1]))
        rospy.Timer(rospy.Duration(0.5), self.timerCallbackForPublishing)
        # Initialise a subscriber
        rospy.Subscriber("/pose_estimate", Pose, self.templateSubscriberCallback)
        rospy.Subscriber("/generateTrajectoryCommand", GenTrajectoryCommand, self.genTrajectoryCallback)
        rospy.Subscriber("/overrideTrajectoryCommand", TrajectoryChunk, self.overrideTrajectoryCallback)

    def genTrajectoryCallback(self, command):
	'''Callback to handle a generate trajectory command receiving a new target point'''
    
        rospy.loginfo("Received command to go to [%f, %f]" % (command.x,command.y))
        self.trajectory_description = []
        self.trajectory_description = genTrajectoryDescription(self.getPath([command.x, command.y]))
	self.start_time = rospy.get_time()
	self.timerCallbackForPublishing(None)
	rospy.loginfo("genTrajectoryCallback success")

    # Respond to timer callback by updating the reference
    def timerCallbackForPublishing(self, event):
	now = rospy.get_time() - self.start_time
	i = 0
	rospy.loginfo("Current time = %f" % now)
	if len(self.trajectory_description) == 0:
	    return
	while self.trajectory_description[i][0]<now:
	    if i == len(self.trajectory_description)-1:
		break
	    i = i + 1

	msg = TrajectoryChunk()
	msg.timestamp = self.trajectory_description[i][0]
	msg.x = 	self.trajectory_description[i][1]
	msg.y = 	self.trajectory_description[i][2]
	msg.phi = 	self.trajectory_description[i][3]
        self.template_publisher.publish(msg)
	
        rospy.loginfo("[TRAJECTORY GEN] current ref: " + str(self.trajectory_description[i]))

    def overrideTrajectoryCallback(self, msg):
        self.trajectory_description = []
        self.trajectory_description.append([msg.timestamp, msg.x, msg.y, msg.phi])
        self.start_time = rospy.get_time()
        self.timerCallbackForPublishing(None)

    # Respond to subscriber receiving a message
    def templateSubscriberCallback(self, msg):
        rospy.loginfo("[TRAJECTORY GEN] Message receieved with data = " + str(msg))
        self.pose[X_INDEX] = msg.x
        self.pose[Y_INDEX] = msg.y
        self.pose[PHI_INDEX] = msg.phi

    def getPath(self, p_end):
        rospy.loginfo("Planning path to " + str(p_end))
        rev_spec = []
        # rev_spec = [[0.0, 0.0], [2.0, 0.0], [3.0, 1.0], [3.0, 2.0], [2.0, 2.0],[2.0, 2.5], [3.0, 3.5], [3.0, 4.5], [0.0, 4.5], [0.0, 0.0]]
        for i in range(len(self.warehouse_specification)):
            rev_spec.append([])
            rev_spec[i].append(self.warehouse_specification[i][0])
            rev_spec[i].append(self.warehouse_specification[i][1])
            
        rospy.loginfo("rev_spec is "+str(rev_spec))
        buffer_dist = 0.2
        point_dist = buffer_dist * 0.5
        N = len(self.warehouse_specification)
        visGraph = Graph(N+1)
        rospy.loginfo("N = "+str(N))
        p_back = self.warehouse_specification[N - 2]
        p = self.warehouse_specification[0]
        p_forward = self.warehouse_specification[1]
        rev_spec[0] = p + get_buffer(p, p_back, p_forward, buffer_dist)
        for i in range(1, (N - 1)):
            # rospy.loginfo("getPath vertex "+str(i) + str(self.warehouse_specification[i]))
            p_back = self.warehouse_specification[i - 1]
            p = self.warehouse_specification[i]
            p_forward = self.warehouse_specification[i + 1]
            # rospy.loginfo("getPath buffer points "+str(p_back)+str(p)+str(p_forward))
            rev_spec[i] = p + get_buffer(p, p_back, p_forward, buffer_dist)

        p_start = [self.pose[0], self.pose[1]]
        if ~doCollideCheck(p_start, p_end, N, self.warehouse_specification):
            if ~checkPointBufferCollide(p_start, p_end, N, self.warehouse_specification, point_dist):
                visGraph.add_edge(N-1, N, norm(np.subtract(p_start, p_end)))
        else:
            for i in range(0, (N - 1)):
                p = rev_spec[i]
                if ~doCollideCheck(p_start, p, N, self.warehouse_specification):
                    if ~checkPointBufferCollide(p_start, p, N, self.warehouse_specification, point_dist):
                        visGraph.add_edge(N-1, i, norm(np.subtract(p, p_start)))
            for i in range(0, (N - 1)):
                p = rev_spec[i]
                if ~doCollideCheck(p_end, p, N, self.warehouse_specification):
                    if ~checkPointBufferCollide(p_end, p, N, self.warehouse_specification, point_dist):
                        visGraph.add_edge(N, i, norm(np.subtract(p, p_end)))

        for n in range(0, (N - 1)):
            p1 = rev_spec[n]
            for m in range(n+1, (N - 1)):
                p2 = rev_spec[m]
                if ~doCollideCheck(p1, p2, N, self.warehouse_specification):
                    if ~checkPointBufferCollide(p1, p2, N, self.warehouse_specification, point_dist):
                        visGraph.add_edge(n, m, norm(np.subtract(p1, p2)))
	path = visGraph.dijkstra(N-1, N)
        rospy.loginfo("path is "+str(path))
        rospy.loginfo("N is "+str(N))
	planned_path = [[self.pose[0], self.pose[1]]]
	for i in path:
	    if i == N:
		planned_path.append(p_end)
		break
	    if i != N-1:
		planned_path.append(rev_spec[i])
        return planned_path


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "trajectoryGen"
    rospy.init_node(node_name)
    template_py_node = trajectoryGen()
    # Spin as a single-threaded node
    rospy.spin()
