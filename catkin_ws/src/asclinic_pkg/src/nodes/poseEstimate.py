#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Template Python node with a publisher and subscriber
#
# ----------------------------------------------------------------------------





# Import the ROS-Python package
import rospy
from math import pi as PI
from math import cos, sin
import numpy as np

# Import the standard message types
from std_msgs.msg import UInt32

# Import the asclinic message types
from asclinic_pkg.msg import Pose
from asclinic_pkg.msg import OdometryPose
from asclinic_pkg.msg import LeftAndRightInt
from constant import wheel_radius_left_mm as wheel_radius_left
from constant import wheel_radius_right_mm as wheel_radius_right
from constant import half_wheel_base_mm as half_wheel_base


INITIAL_X = 0
INITIAL_Y = 0
INITIAL_phi = 0
SHOW_OUTPUT = True
# NOTE:
# A number of the function description given in this file are
# taken directly from the following ROS tutorials:
# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29
# http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers
# http://wiki.ros.org/rospy/Overview/Time

K_L = 0.05**2
K_R = 0.05**2

def getPoseMatrixFromMessage(pose_msg):
    poseMatrix = np.zeros((3,1))
    poseMatrix[0] = pose_msg.x
    poseMatrix[1] = pose_msg.y
    poseMatrix[2] = pose_msg.phi
    return poseMatrix

def getPoseMessageFromMatrix(pose_matrix):
    pose_msg = Pose()
    pose_msg.x = pose_matrix[0]
    pose_msg.y = pose_matrix[1]
    pose_msg.phi = pose_matrix[2]
    return pose_msg

class PoseEstimator:

    def __init__(self, namespace):
        self.namespace = namespace
        self.odometry_cov_previous = np.eye(3)
        self.odometry_pose_covariance = np.zeros((3, 3))
        self.camera_pose_covariance = np.eye(3)
        self.previous_pose_covariance = np.zeros((3, 3))
        self.previous_pose_prediction = np.zeros((3,1))
        
        previous_pose = np.zeros((3,1))
        previous_pose[0] = INITIAL_X
        previous_pose[1] = INITIAL_Y
        previous_pose[2] = INITIAL_phi
        
        self.last_odometry_pose = previous_pose

        self.previous_pose = previous_pose
        # INITIALISE THE PUBLISHER
        self.pose_camera_subscriber = rospy.Subscriber(namespace + "pose_estimate_camera", Pose, self.poseEstimateCameraCallback)
        self.pose_odometry_subscriber = rospy.Subscriber(namespace + "pose_estimate_odometry", OdometryPose, self.poseEstimateOdometryCallback)
        self.pose_publisher = rospy.Publisher("/pose_estimate", Pose, queue_size=5)
        rospy.loginfo("[Pose Estimate] Setup Finished ")



   



    def updatePose(self, Kt, camera_prediction):
        self.previous_pose = self.last_odometry_pose + np.matmul(Kt, camera_prediction - self.last_odometry_pose)
        pose_msg = getPoseMessageFromMatrix(self.previous_pose)
        if SHOW_OUTPUT:
            rospy.loginfo("[Pose Estimate: update pose] Last odo pose: " + "x = " + str(self.last_odometry_pose[0]) + " , y = " + str(self.last_odometry_pose[1]) + " , phi" + str(self.last_odometry_pose[2]/PI) + "PI \n " )
            rospy.loginfo("[Pose Estimate] Current pose: " + "x = " + str(pose_msg.x) + " , y = " + str(pose_msg.y) + " , phi" + str(pose_msg.phi/PI) + "PI \n " )
        self.pose_publisher.publish(pose_msg)
        
    def updateCovariance(self, Kt): #update the covariance of pose
        self.previous_pose_covariance = self.odometry_pose_covariance - np.matmul(Kt, np.matmul((self.odometry_pose_covariance - self.camera_pose_covariance), Kt.T))


    # Respond to subscriber receiving a message
    def poseEstimateCameraCallback(self, pose):
        # Display that a message was received
        if SHOW_OUTPUT:
            rospy.loginfo("[Pose Estimate] Camera: " + "x = " + str(pose.x) + " , y = " + str(pose.y) + " , phi" + str(pose.phi/PI) + "PI \n " )

        camera_prediction = getPoseMatrixFromMessage(pose)
        #Update Kalman gain 
        Kt = np.matmul(self.previous_pose_covariance, np.linalg.inv(self.previous_pose_covariance + self.camera_pose_covariance))
        if SHOW_OUTPUT:
            rospy.loginfo("[Pose Estimate: Camera] Kt: " + str(Kt))

        #update pose
        self.updatePose(Kt, camera_prediction)
        #update Covariance

        self.updateCovariance(Kt)
        
     # Respond to subscriber receiving a message
    

    def updateOdometryCovariance(self, pose):
        delta_x = pose.delta_x
        delta_y = pose.delta_y 
        delta_phi = pose.delta_phi
        delta_theta_l = pose.delta_theta_l
        delta_theta_r = pose.delta_theta_r
        last_odometry_pose_local = self.last_odometry_pose
        x_previous = last_odometry_pose_local[0]
        y_previous = last_odometry_pose_local[1]
        phi_previous = last_odometry_pose_local[2]

        u = (wheel_radius_left*delta_theta_l + wheel_radius_right*delta_theta_r)/2
        v = (wheel_radius_right*delta_theta_r - wheel_radius_left*delta_theta_l)/(2*half_wheel_base)
        angle_arg = phi_previous + 0.5*v

        theta_covariance = np.zeros((2, 2))
        theta_covariance[0,0] = K_L*abs(delta_theta_l)
        theta_covariance[1,1] = K_R*abs(delta_theta_r)

        gradTheta = np.array([
            [(wheel_radius_left/2)*cos(angle_arg) + (wheel_radius_left/(4*half_wheel_base))*(u*sin(angle_arg)),
            (wheel_radius_left/2)*sin(angle_arg) - (wheel_radius_left/(4*half_wheel_base))*(u*cos(angle_arg)),
            -wheel_radius_left/(2*half_wheel_base)],
            [(wheel_radius_right/2)*cos(angle_arg) - (wheel_radius_right/(4*half_wheel_base))*(u*sin(angle_arg)),
            (wheel_radius_right/2)*sin(angle_arg) + (wheel_radius_right/(4*half_wheel_base))*(u*cos(angle_arg)),
            wheel_radius_right/(2*half_wheel_base)] 
            ])

        gradp = np.array([[1, 0, 0], [0, 1 ,0], [-u*sin(angle_arg), u*cos(angle_arg), 1]], order='F')
        stateCov = np.matmul(gradp,np.matmul(self.odometry_cov_previous, gradp.T))
        thetaCov = np.matmul(gradTheta.T, np.matmul(theta_covariance, gradTheta))

        odometry_pose_covariance = stateCov + thetaCov
        self.odometry_cov_previous = odometry_pose_covariance


        return odometry_pose_covariance



    def poseEstimateOdometryCallback(self, pose):
        # Display that a message was received
        if SHOW_OUTPUT:
            rospy.loginfo("[Pose Estimate] Odometry: " + "delta_x = " + str(pose.delta_x) + " , delta_y = " + str(pose.delta_y) + " , delta_phi" + str(pose.delta_phi/PI) + "PI \n " )
        delta_x = pose.delta_x
        delta_y = pose.delta_y 
        delta_phi = pose.delta_phi
        delta_theta_l = pose.delta_theta_l
        delta_theta_r = pose.delta_theta_r
       
        self.odometry_pose_covariance = self.updateOdometryCovariance(pose)


        #Store the previous pose state
        #update pose 
        current_pose = np.copy(self.last_odometry_pose)
        current_pose[0] += delta_x
        current_pose[1] += delta_y
        current_pose[2] += delta_phi
        if SHOW_OUTPUT:
            rospy.loginfo("[Pose Estimate]Updating Odometry: " + "x = " + str(current_pose[0]) + " , y = " + str(current_pose[1]) + " , phi" + str(current_pose[2]/PI) + "PI \n " )

        yeet_pose = Pose()
        yeet_pose.x = current_pose[0]
        yeet_pose.y = current_pose[1]
        yeet_pose.phi = current_pose[2]
        self.last_odometry_pose = current_pose
        self.pose_publisher.publish(yeet_pose) 


if __name__ == '__main__':
    

    # STARTING THE ROS NODE
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    global node_name
    node_name = "poseEstimate"
    rospy.init_node(node_name, anonymous=False)
    namespace = rospy.get_namespace()
    rospy.loginfo("[Pose Estimate] Namespace == " + namespace)
    poseEstimator = PoseEstimator(namespace)
    
    if rospy.has_param(namespace + '/initial_phi'):
        INITIAL_phi = rospy.get_param(namespace + '/initial_phi')
        rospy.loginfo("[Pose Esimate]: Found parameter initial phi as " + str(INITIAL_phi))
    if rospy.has_param(namespace + '/initial_X'):
         INITIAL_X = rospy.get_param(namespace + '/initial_X')
         rospy.loginfo("[Pose Esimate]: Found parameter initial X as " + str(INITIAL_X))
    if rospy.has_param(namespace + '/initial_Y'):
         INITIAL_Y = rospy.get_param(namespace + '/initial_Y')
         rospy.loginfo("[Pose Esimate]: Found parameter initial Y as " + str(INITIAL_Y))
    
     
    # SPIN AS A SINGLE-THREADED NODE
    #
    # rospy.spin() will enter a loop, pumping callbacks.  With this version, all
    # callbacks will be called from within this thread (the main one).  ros::spin()
    # will exit when Ctrl-C is pressed, or the node is shutdown by the master.
    #
    rospy.spin()
