#include <stdint.h>

const int wheel_radius_left_mm = 72;
const int wheel_radius_right_mm = 72;
const int half_wheel_base_mm = 109;
const int X_INDEX = 0;
const int Y_INDEX = 1;
const int PHI_INDEX = 2;
