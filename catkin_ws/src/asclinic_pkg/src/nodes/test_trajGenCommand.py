#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from asclinic_pkg.msg import TrajectoryChunk, GenTrajectoryCommand

class test_trajGenCommand:

    def __init__(self):
        # Initialise a publisher
        self.publisher = rospy.Publisher(node_name + "/generateTrajectoryCommand", GenTrajectoryCommand, queue_size=10)
        # Initialise pose
        self.msg = GenTrajectoryCommand()
        self.msg.x = 1.0
        self.msg.y = 1.0
        # self.template_publisher.publish(self.msg)

    def run(self):
        self.publisher.publish(self.msg)
        rospy.loginfo("Published genTrajCommand")

if __name__ == '__main__':
    global node_name
    node_name = "test_trajGenCommand"
    rospy.init_node(node_name)
    m = test_trajGenCommand()
    rate = rospy.Rate(10)  # 10hz
    while not rospy.is_shutdown():
        connections = m.publisher.get_num_connections()
        rospy.loginfo('Connections: %d', connections)
        if connections > 0:
            m.run()
            rospy.loginfo('Published')
            break
        rate.sleep()
