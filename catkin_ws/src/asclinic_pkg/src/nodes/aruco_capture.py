#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Python node to detect ArUco markers in the camera images
#
# ----------------------------------------------------------------------------



# ----------------------------------------------------------------------------
# A FEW USEFUL LINKS ABOUT ARUCO MARKER DETECTION
#
# > This link is most similar to the code used in this node:
#   https://aliyasineser.medium.com/aruco-marker-tracking-with-opencv-8cb844c26628
#
# > This online tutorial provdes very detailed explanations:
#   https://www.pyimagesearch.com/2020/12/21/detecting-aruco-markers-with-opencv-and-python/
#
# > This link is the main Aruco website:
#   https://www.uco.es/investiga/grupos/ava/node/26
# > And this is the documentation as linked on the Aruco website
#   https://docs.google.com/document/d/1QU9KoBtjSM2kF6ITOjQ76xqL7H0TEtXriJX5kwi9Kgc/edit
#
# > This link is an OpenCV tutorial for detection of ArUco markers:
#   https://docs.opencv.org/master/d5/dae/tutorial_aruco_detection.html
#
# > As starting point for details about Rodrigues representation of rotations
#   https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
#
# ----------------------------------------------------------------------------



# Import the ROS-Python package
import rospy
from math import atan2
from math import pi as PI
#import rospkg

# Import the standard message types
from std_msgs.msg import UInt32
from sensor_msgs.msg import Image
from asclinic_pkg.msg import Pose

# Import numpy
import numpy as np

# Import opencv
import cv2

# Import aruco
import cv2.aruco as aruco

# Package to convert between ROS and OpenCV Images
from cv_bridge import CvBridge



# DEFINE THE PARAMETERS
# > For the number of the USB camera device
USB_CAMERA_DEVICE_NUMBER = 0
INTRINSIC_CAMERA_MATRIX = [[1.70380430e03, 0.00000000e00, 1.14484411e03], [0.00000000e00, 1.70408071e03, 7.50320468e02],[0.00000000e00, 0.00000000e00, 1.00000000e00]]
INTRINSIC_CAMERA_DISTORTION = [[ 0.04706318, -0.19091073, -0.00394028, -0.00326405, 0.14914337]]
MARKER_TO_WORLD_ROT = [0,PI, PI/2, PI/2, PI/2, 0, 0, 0, 0, 3*PI/2, 3*PI/2, 3*PI/2, PI/2, PI/2, 0, 0, 0,0] #added extra for id 0
MARKER_TO_WORLD_TRANS = [[0,0], [400,2400], [0,2000],[0,1200],[0,400],[400,0],[1200,0],[2000,0],[2800,0],[3200,400],[3200,1200],[3200,2000],[2800,2400], [2000,2400],[400,2400],[2000,2400],[2800,2400],[0,0]]

# > For the size of the aruco marker, in mm
MARKER_SIZE = 151 #0.151m
# > For where to save images captured by the camera
#   Note: ensure that this path already exists
#   Note: images are only saved when a message is received
#         on the "request_save_image" topic.
SAVE_IMAGE_PATH = "/home/asc12/saved_camera_images/"
# > A flag for whether to display the images captured
SHOULD_SHOW_IMAGES = False
SHOW_OUTPUT = True

def wrap_2_pi(phi):
    if 0 <= phi <= 2*PI:
        return phi
    if phi < 0: 
        return wrap_2_pi(phi+2*PI)
    if phi > 2*PI:
        return wrap_2_pi(phi-2*PI) 

    
    
#from https://stackoverflow.com/questions/20840692/rotation-of-a-2d-array-over-an-angle-using-rotation-matrix
def rotate(vector, theta, rotation_around=None) -> np.ndarray:
    """
    reference: https://en.wikipedia.org/wiki/Rotation_matrix#In_two_dimensions
    :param vector: list of length 2 OR
                   list of list where inner list has size 2 OR
                   1D numpy array of length 2 OR
                   2D numpy array of size (number of points, 2)
    :param theta: rotation angle in degree (+ve value of anti-clockwise rotation)
    :param rotation_around: "vector" will be rotated around this point,
                    otherwise [0, 0] will be considered as rotation axis
    :return: rotated "vector" about "theta" degree around rotation
             axis "rotation_around" numpy array
    """
    vector = np.array(vector)

    if vector.ndim == 1:
        vector = vector[np.newaxis, :]

    if rotation_around is not None:
        vector = vector - rotation_around

    vector = vector.T
    vector = vector.reshape((2,1))
    

    theta = theta

    rotation_matrix = np.array([
        [np.cos(theta), -np.sin(theta)],
        [np.sin(theta), np.cos(theta)]
    ])

    output: np.ndarray = (rotation_matrix @ vector).T

    if rotation_around is not None:
        output = output + rotation_around

    return output.squeeze()



class ArucoDetector:

    def __init__(self, namespace):
        self.namespace = namespace
        
        # Initialise a publisher for the images
        self.image_publisher = rospy.Publisher("/global_namespace"+"/camera_image", Image, queue_size=10)

        # Initialise a subscriber for flagging when to save an image
        rospy.Subscriber(namespace+ "request_save_image", UInt32, self.requestSaveImageSubscriberCallback)
        self.pose_publisher = rospy.Publisher(namespace + "pose_estimate_camera", Pose, queue_size=5)

        # > For convenience, the command line can be used to trigger this subscriber
        #   by publishing a message to the "request_save_image" as follows:
        #
        # rostopic pub /global_namespace/request_save_image std_msgs/UInt32 "data: 1" 

        # Initialise varaibles for managing the saving of an image
        self.save_image_counter = 0
        self.should_save_image = True

        # Specify the details for camera to capture from

        # > For capturing from a USB camera:
        #   > List the content of /dev/video* to determine
        #     the number of the USB camera
        self.camera_setup = USB_CAMERA_DEVICE_NUMBER

        #   > Gstreamer for capture video
        #   > sensor-id=0 for CAM0 and sensor-id=1 for CAM1
        #   > This is not optimized, but it does work.
        #self.camera_setup = 'nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=3264, height=2464, framerate=12/1, format=NV12 ! nvvidconv flip-method=0 ! video/x-raw, width = 800, height=600, format =BGRx ! videoconvert ! video/x-raw, format=BGR ! appsink'

        # Initialise video capture from the camera
        self.cam=cv2.VideoCapture(self.camera_setup)

        # Initlaise the OpenCV<->ROS bridge
        self.cv_bridge = CvBridge()

        # Get the ArUco dictionary to use
        self.aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)

        # Create an parameter structure needed for the ArUco detection
        self.aruco_parameters = aruco.DetectorParameters_create()
        # > Specify the parameter for: corner refinement
        self.aruco_parameters.cornerRefinementMethod = aruco.CORNER_REFINE_SUBPIX

        # Specify the intrinsic parameters of the camera
        # > These parameters could either be hardcoded here;
        # > Or you can load then from a file that you may have
        #   saved during the calibration procedure.
        # > Note the that values hardcoded in this template
        #   may give meaningless results for your camera
        self.intrinic_camera_matrix = np.array(INTRINSIC_CAMERA_MATRIX, dtype=float)
        self.intrinic_camera_distortion  = np.array(INTRINSIC_CAMERA_DISTORTION, dtype=float)

        # Display the status
        rospy.loginfo("[ARUCO DETECTOR] Initialisation complete")

        # Initialise a timer for capturing the camera frames
        rospy.Timer(rospy.Duration(3.0), self.timerCallbackForPublishing)



    # Respond to timer callback
    def timerCallbackForPublishing(self, event):
        # Read the camera frame
        #rospy.loginfo('[TEMPLATE ARUCO DETECTOR] Now reading camera frame')
        return_flag , current_frame = self.cam.read()

        # Check if the camera frame was successfully read
        if (return_flag == True):
            # Convert the image to gray scale
            current_frame_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)

            # Detect ArUco markers from the frame
            aruco_corners_of_all_markers, aruco_ids, aruco_rejected_img_points = aruco.detectMarkers(current_frame_gray, self.aruco_dict, parameters=self.aruco_parameters)

            # Process any ArUco markers that were detected
            if aruco_ids is not None:
                # Display the number of markers found
                #rospy.loginfo("[ARUCO DETECTOR] ArUco marker found, len(aruco_ids) = " + str(len(aruco_ids)) )
                # Outline all of the markers detected found in the image
                current_frame_with_marker_outlines = aruco.drawDetectedMarkers(current_frame.copy(), aruco_corners_of_all_markers, aruco_ids, borderColor=(0, 220, 0))
                number_of_markers_detected = len(aruco_ids)
                average_results = np.zeros((3,1))

                # Iterate over the markers detected
                for i_marker_id in range(number_of_markers_detected):
                    # Get the ID for this marker
                    this_id = int(aruco_ids[i_marker_id])
                    if this_id > 17:
                        rospy.loginfo("[ARUCO DETECTOR] Invalid ID " + str(this_id) + "Detected")
                        return

                    # Get the corners for this marker
                    corners_of_this_marker = aruco_corners_of_all_markers[i_marker_id]
                    # Estimate the pose of this marker
                    # > Note: ensure that the intrinsic parameters are
                    #   set for the specific camera in use.
                    this_rvec_estimate, this_tvec_estimate, _objPoints = aruco.estimatePoseSingleMarkers(corners_of_this_marker, MARKER_SIZE, self.intrinic_camera_matrix, self.intrinic_camera_distortion)
                    # Draw the pose of this marker
                    rvec = this_rvec_estimate[0]
                    tvec = this_tvec_estimate[0]
                    current_frame_with_marker_outlines = aruco.drawAxis(current_frame_with_marker_outlines, self.intrinic_camera_matrix, self.intrinic_camera_distortion, rvec, tvec, MARKER_SIZE)
                    rvec = rvec[0]
                    tvec = tvec[0]
                    
                    # At this stage, the variable "rvec" and "tvec" respectively
                    # describe the rotation and translation of the marker frame
                    # relative to the camera frame, i.e.:
                    # tvec - is a vector of length 3 expressing the (x,y,z) coordinates
                    #        of the marker's center in the coordinate frame of the camera.
                    # rvec - is a vector of length 3 expressing the rotation of the marker's
                    #        frame relative to the frame of the camera.
                    #        This vector is an "axis angle" respresentation of the rotation.
                    #        As y_marker axis is always parallel (axis of rotation), the only rotation is the x axis and z axis
                    #        Therefore we throw away the y value we get from the pose estimate

                    Rmat = np.matrix(cv2.Rodrigues(rvec)[0])
                    Rinv = np.linalg.inv(Rmat)                
                    camera_in_marker = np.matrix(Rinv).dot(np.matrix(-tvec).T)
                    # if SHOW_OUTPUT:
                    #     rospy.loginfo("[ARUCO DETECTOR] for id = " + str(this_id) + ", c_i_m 0 = [ " + str(camera_in_marker[0]) + " , " + str(camera_in_marker[1]) + " , " + str(camera_in_marker[2]) + " ]" )
                    #camera_in_marker[0] = camera_in_marker[0]
                    camera_in_marker_2D = [camera_in_marker[0], camera_in_marker[2]]
                    world = np.array(MARKER_TO_WORLD_TRANS[this_id]) + rotate(camera_in_marker_2D, -MARKER_TO_WORLD_ROT[this_id])
                    phi = wrap_2_pi(rvec[1] + -MARKER_TO_WORLD_ROT[this_id] + PI/2)
                    x = world[0]
                    y = world[1]





                    # Compute the rotation matrix friom the rvec using the Rodrigues
                    #Rmat = cv2.Rodrigues(rvec)

                    # A vector expressed in the camera coordinates can now be
                    # rotated to the marker coordinates as:
                    #camera to marker   


                    # Note: the camera frame convention is:
                    # > z-axis points along the optical axis, i.e., straight out of the lens
                    # > x-axis points to the right when looking out of the lens along the z-axis
                    # > y-axis points to the down  when looking out of the lens along the z-axis

                    # Display the rvec and tvec
                    if SHOW_OUTPUT:
                        rospy.loginfo("[ARUCO DETECTOR] for id = " + str(this_id) + ", tvec = [ " + str(tvec[0]) + " , " + str(tvec[1]) + " , " + str(tvec[2]) + " ]" )
                        rospy.loginfo("[ARUCO DETECTOR] for id = " + str(this_id) + ", rvec = [ " + str(rvec[0]) + " , " + str(rvec[1]) + " , " + str(rvec[2]) + " ] \n" )
                   
                    # ============================================
                    # TO BE FILLED IN FOR WORLD FRAME LOCALISATION
                    # ============================================
                    # Based on the known location and rotation of
                    # marker relative to the world frame, compute
                    # an estimate of the camera's location within
                    # the world frame, and hence an estimate of
                    # robot's pose on which the camera is mounted. 
                    #
                    # ADD YOUR CODE HERE
                    #
                    # PUBLISH THE ESTIMATE OF THE ROBOT'S POSE
                    # FOR USE BY OTHER ROS NODES
                    #
                    # ============================================

                    rospy.loginfo("[ARUCO DETECTOR] for id = " + str(this_id) + ", x = " + str(x) + " , y = " + str(y) + " , phi" + str(phi/PI) + "PI \n " )
                    average_results[0] += x
                    average_results[1] += y
                    average_results[2] += phi

                average_results = average_results/number_of_markers_detected
                pose = Pose()
                pose.x = average_results[0]/1000
                pose.y = average_results[1]/1000
                pose.phi = average_results[2]

                self.pose_publisher.publish(pose)


            else:
                # Display that no aruco markers were found
                if SHOW_OUTPUT:
                    rospy.loginfo("[ARUCO DETECTOR] No markers found in this image")

                current_frame_with_marker_outlines = current_frame_gray

            # Publish the camera frame
            #rospy.loginfo('[ARUCO DETECTOR] Now publishing camera frame')
            self.image_publisher.publish(self.cv_bridge.cv2_to_imgmsg(current_frame))

            # Save the camera frame if requested
            if (self.should_save_image):
                # Increment the image counter
                self.save_image_counter += 1
                # Write the image to file
                temp_filename = SAVE_IMAGE_PATH + "image" + str(self.save_image_counter) + ".jpg"
                cv2.imwrite(temp_filename,current_frame_with_marker_outlines)
                # Display the path to where the image was saved
                if SHOW_OUTPUT:
                    rospy.loginfo("[ARUCO DETECTOR] Save camera frame to: " + temp_filename)
                # Reset the flag to false
                self.should_save_image = False

            # Display the camera frame if requested
            if (SHOULD_SHOW_IMAGES):
                rospy.loginfo("[ARUCO DETECTOR] Now displaying camera frame")
                cv2.imshow("CAM 0", current_frame_with_marker_outlines)

        else:
            # Display an error message
            rospy.loginfo('[ARUCO DETECTOR] ERROR occurred during self.cam.read()')



    # Respond to subscriber receiving a message
    def requestSaveImageSubscriberCallback(self, msg):
        rospy.loginfo("[ARUCO DETECTOR] Request receieved to save the next image")
        # Set the flag for saving an image to true
        self.should_save_image = True



if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "aruco_detector"
    rospy.init_node(node_name)
    namespace = rospy.get_namespace()
    aruco_detector = ArucoDetector(namespace)
    # Spin as a single-threaded node
    rospy.spin()

    # Release the camera
    aruco_detector.cam.release()
    # Close any OpenCV windows
    cv2.destroyAllWindows()
