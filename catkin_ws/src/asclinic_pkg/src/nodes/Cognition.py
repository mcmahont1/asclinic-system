#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from asclinic_pkg.msg import GenTrajectoryCommand
# ADD: Import Messages
from std_msgs.msg import Bool
from asclinic_pkg.msg import TrajectoryChunk, GenTrajectoryCommand, Pose
from enum import Enum

CLOCKRATE = 50 # Hz
QUERYSUBSCOUNTRATE = 50  # Hz
MINDISTARRIVED = 0.01
MINDISTCOLLIDE = 10
MAXDISTTRACKING = 5
STAGE_START = 0
STAGE_PICKUP = 1
STAGE_DROPOFF = 2


# GTC is generateTrajectoryCommand
class State(Enum):
    Off = 0                   # Off state
    Idle = 1                   # Waiting to begin
    GetLocEst = 2              # Get initial location estimate
    ImproveLocEst = 3          # Improve location estimate if error too large
    GenerateTrajectory = 4     # Generate a trajectory to the target
    FollowTrajectory = 5       # Follow pose reference
    AvoidObject = 6            # Adjust trajectory to avoid object
    Arrived = 7                # Arrived at target
    PickupCargo = 8            # Arrived at cargo, pick it up
    PutDownCargo = 9           # Arrived at delivery, put it down
    MissionComplete = 10        # Completed delivery, go back to waiting for mission (Idle/Off)
    FailState = 11              # Go here if failure detected, log stuff?


class Cognition:
    def __init__(self):
        # Initialise targets
        self.cargoLoc = GenTrajectoryCommand()
        self.cargoLoc.x = 1.0
        self.cargoLoc.y = 2.0
        self.truckLoc = GenTrajectoryCommand()
        self.truckLoc.x = 3.0
        self.truckLoc.y = 3.0
        self.target = GenTrajectoryCommand()
        self.target.x = 0.0  # Default
        self.target.y = 0.0  # Default
        self.motorEnabled = False

        self.state = State.Off
        self.tofDist = 1000.0
        self.stage = 0 # 0 is start, 1 is go to cargo, 2 is dropoff cargo
        self.clockRatePeriod = 1.0/CLOCKRATE # Hz
        self.rate = rospy.Rate(CLOCKRATE)
        self.Updating = 0
        self.pose = Pose()
        # Initialise a subscriber to the pose estimate
        self.poseSubscriber = rospy.Subscriber("/pose_estimate", Pose, self.poseUpdateCallback)  # Update this namespace
        # Initialise a publisher
        self.GTCPublisher = rospy.Publisher("/generateTrajectoryCommand", GenTrajectoryCommand, queue_size=10)
        self.putDownCommandPublisher = rospy.Publisher("/putDownCommand", Bool, queue_size=10)
        self.pickUpCommandPublisher = rospy.Publisher("/pickUpCommand", Bool, queue_size=10)
        self.motorStartStopCommandPublisher = rospy.Publisher("/motorStartStopCommand", Bool, queue_size=10) #1=on, 0=off
        self.localiseCommandPublisher = rospy.Publisher("/localiseCommand", Bool, queue_size=10)
        self.localiseCommandPublisher = rospy.Publisher("/overrideTrajectoryCommand", TrajectoryChunk, queue_size=10)
        # self.template_publisher.publish(self.msg)
        self.topicList = [self.poseSubscriber, self.GTCPublisher, self.motorStartStopCommandPublisher]
        rospy.Timer(rospy.Duration(self.clockRatePeriod), self.callback)
        # rospy.Timer(rospy.Duration(1.0/self.clockRate), self.callback)

    def callback(self, event):
        if ~self.Updating:
            self.Updating = 1
            self.stateTransitions()
            self.Updating = 0
        else:
            rospy.loginfo("ERROR:" + node_name + " clockRate too fast")

    def poseUpdateCallback(self, msg):
        self.pose = msg

    def stateTransitions(self):
        # Get a template message ready
        msg = Bool()
        msg.data = True
        if self.state == State.Off:
            
            rospy.loginfo("[COGNITION] state is: OFF")
            self.state = State.Idle
            # Check for on/start command, move to idle
        elif self.state == State.Idle:
            
            rospy.loginfo("[COGNITION] state is: IDLE")
            # check for connections
            self.stage = STAGE_PICKUP
            self.target = self.cargoLoc
            self.state = State.GetLocEst
            for topic in self.topicList:
                connections = topic.get_num_connections()
                if connections == 0:
                    self.state = State.Idle
                    break
            # Initialise and move to GetLocEst
            self.motorEnabled = False
            msg.data = False
            self.motorStartStopCommandPublisher.publish(msg)
            # Set target, initialise nodes if need be (and check they're running?)

        elif self.state == State.GetLocEst:
            rospy.loginfo("[COGNITION] state is: GetLocEst")
            self.state = State.GenerateTrajectory
            # Get location estimate
            # Check if error is low enough, move to ImproveLocEst or GenerateTrajectory accordingly

        elif self.state == State.ImproveLocEst:
            rospy.loginfo("[COGNITION] state is: ImproveLocEst")
            self.state = State.GenerateTrajectory
            pass
            # Do something to improve location estimate, then return to GetLocEst? (or GenerateTrajectory)

        elif self.state == State.GenerateTrajectory:
            rospy.loginfo("[COGNITION] state is: GenerateTrajectory")
            # Send command to generate the trajectory
            if self.stage == STAGE_PICKUP:
                self.target = self.cargoLoc
            elif self.stage ==STAGE_DROPOFF:
                self.target = self.truckLoc
            else:
                return
            
            if self.renewTarget():
                self.state = State.FollowTrajectory
            else:
                self.state = State.FailState
            # Move to FollowTrajectory
            
        elif self.state == State.FollowTrajectory:
            
            rospy.loginfo("[COGNITION] state is: FollowTrajectory")
            # Check collision detection, if so move to AvoidObject
            if self.tofDist < MINDISTCOLLIDE:
                rospy.loginfo("[COGNITION] tof sensor activated, going to collision avoid!")
                msg.data = False
                self.motorEnabled = False
                self.motorStartStopCommandPublisher.publish(msg)
                self.state = State.AvoidObject
            # Else check distance from reference, if too far, move to GenerateTrajectory
            elif (self.target.x-self.pose.x)**2 + (self.target.y-self.pose.y)**2 > MAXDISTTRACKING**2:
                rospy.loginfo("[COGNITION] too far from reference!")
                msg.data = False
                self.motorEnabled = False
                self.motorStartStopCommandPublisher.publish(msg)
                self.state = State.GenerateTrajectory
            # Else check distance from target, if close enough arrive
            elif (self.target.x-self.pose.x)**2 + (self.target.y-self.pose.y)**2 < MINDISTARRIVED**2:
                rospy.loginfo("[COGNITION] arrived at reference!")
                msg.data = False
                self.motorEnabled = False
                self.motorStartStopCommandPublisher.publish(msg)
                self.state = State.Arrived
            # Else enable movement
            elif self.motorEnabled==False:
                msg.data = True
                self.motorEnabled = True
                self.motorStartStopCommandPublisher.publish(msg)

        elif self.state == State.AvoidObject:
            rospy.loginfo("[COGNITION] avoid object state entered")
            LOGSENT = 1
            self.state = State.FailState
            pass
        elif self.state == State.Arrived:
            if self.stage == STAGE_PICKUP:
                self.target = self.truckLoc
                self.state = State.PickupCargo
            elif self.stage == STAGE_DROPOFF:
                self.state = State.PutDownCargo
        elif self.state == State.PickupCargo:
            #Tell trajGen to reposition and then send pickup cargo command
            pass
        elif self.state == State.PutDownCargo:
            #Tell trajGen to reposition and then send pickup cargo command
            pass
        elif self.state == State.MissionComplete:
            pass
        elif self.state == State.FailState:
            msg.data = False
            self.motorStartStopCommandPublisher.publish(msg)
            if LOGSENT == 0:
                rospy.loginfo("[COGNITION] entered FailState, stopping motors")
            pass
        else:
            # Default
            rospy.loginfo("ERROR: Unknown state")
            self.state = State.Off

    def renewTarget(self):
        rate = rospy.Rate(QUERYSUBSCOUNTRATE)  # 10hz
        count = 0
        while not rospy.is_shutdown():
            if count>50:
                rospy.loginfo('[COGNITION] renewTarget timedout after 50 attempts to connect.')
                return False
            count = count + 1
            connections = self.GTCPublisher.get_num_connections()
            rospy.loginfo('[COGNITION] Connections: %d', connections)
            if connections > 0:
                self.GTCPublisher.publish(self.target)
                rospy.loginfo('[COGNITION] Published genTrajCommand to go to '+ str(self.target))
                return 1
            rate.sleep()

if __name__ == '__main__':
    global node_name
    node_name = "Cognition"
    rospy.init_node(node_name)
    cognitionNode = Cognition()
    rospy.spin()
