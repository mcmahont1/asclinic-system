// Include some useful libraries
#include <math.h>
#include <stdlib.h>

// Include the ROS libraries
#include "ros/ros.h"
#include <ros/package.h>

// Include the standard message types
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Bool.h"
#include <std_msgs/String.h>

// Include the asclinic message types
#include "asclinic_pkg/ErrorMessage.h"
#include "asclinic_pkg/ThetaDotMessage.h"
#include "asclinic_pkg/Pose.h"
#include "asclinic_pkg/PWMMessage.h"
#include "asclinic_pkg/LeftAndRightInt.h"

// Namespacing the package
using namespace asclinic_pkg;

#define S_TO_MIN 60.0f
#define RPM_TO_RADS 0.104719f
float counts_per_rev = 1120.0f;


// -----------------------------------------------------------
// V   V    A    RRRR   III    A    BBBB   L      EEEEE   SSSS
// V   V   A A   R   R   I    A A   B   B  L      E      S    
// V   V  A   A  RRRR    I   A   A  BBBB   L      EEE     SSS 
//  V V   AAAAA  R  R    I   AAAAA  B   B  L      E          S
//   V    A   A  R   R  III  A   A  BBBB   LLLLL  EEEEE  SSSS 
// -----------------------------------------------------------

// Publisher variable:
ros::Publisher m_pwm_publisher;
// ros::Publisher m_error_message_publisher;



// Timer variable used for recurrent publishing:
ros::Timer m_timer_for_publishing;
ros::Subscriber motorStartStopCommandSubscriber;

float updated_theta_dot_l = 0.0;
float updated_theta_dot_r = 0.0;
float theta_dot_r_ref = 0.0;
float theta_dot_l_ref = 0.0;

float measured_theta_dot_l = 0.0;
float measured_theta_dot_r = 0.0;

bool motorEnable = false;



// > Callback run when timer fires
void timerCallbackForPublishing(const ros::TimerEvent&);

// > Callback run when subscriber receives a message
void errorSubscriberCallback(const ThetaDotMessage& error);

void motorStartStopCommandCallback(const std_msgs::Bool& msg);



float theta_to_PWM(float ang_vel, std::string flag){
	float m, c, pwm;
	//scale anf_vel for easier computation
	ang_vel = 100*ang_vel;

	// ang_vel = 12/7*pwm-54/7
	// multiplied by -1 for direction (now handles in timerCallback)
	if(flag == "right"){
		m = -12.0/7.0;
		c = -54.0/7.0;
	}else{
	// ang_vel = 113/70*pwm-54/7
		m = 113.0/70.0;
		c = -54.0/7.0;
	}

	pwm = (ang_vel - c)/(1.0*m);

	return pwm;
}


// > Callback run when time fires
void timerCallbackForPublishing(const ros::TimerEvent&){

	float kp  = 12; //12

	// Declare a static integer for publishing a different
	// number each time
	static uint counter = 0;

	// Increment the counter
	counter++;

	// Publish message
	// ThetaDotMessage theta_dot_error = ThetaDotMessage();

	float error_left = theta_dot_l_ref - measured_theta_dot_l;
	float error_right = theta_dot_r_ref - measured_theta_dot_r;
	ROS_INFO_STREAM("[MOTOR CONTROLLER] timerCallbackForPublishing left ref/measured are: ("<<theta_dot_l_ref<<", "<<measured_theta_dot_l<<")");
	ROS_INFO_STREAM("[MOTOR CONTROLLER] timerCallbackForPublishing right ref/measured are: ("<<theta_dot_r_ref<<", "<<measured_theta_dot_r<<")");

	// m_error_message_publisher.publish(theta_dot_error);

	float v_left = kp*(error_left);
	float v_right = kp*(error_right);

    // Calculate conversion from theta_dot_l and theta_dot_r into pwm for right and left wheels

	float pwm_l = v_left*theta_to_PWM(error_left, "left");
	float pwm_r = v_right*theta_to_PWM(error_right, "right");
	
	if (abs(pwm_l)<15) pwm_l=0; //Moved the thing here
	if (abs(pwm_r)<15) pwm_r=0;
	
	ROS_INFO_STREAM("[MOTOR CONTROLLER] timerCallbackForPublishing pwms are: ("<<pwm_l<<", "<<pwm_r<<")");



	// PUBLISH A MESSAGE
	// Initialise a "TemplateMessage" struct
	// > Note that all value are set to zero by default
	// Set the values
	PWMMessage pwm_msg = PWMMessage();
	// std_msgs::Float32 pwm_msg_l;
	pwm_msg.v_r = pwm_l;
	// std_msgs::Float32 pwm_msg_r;
	pwm_msg.v_l = pwm_r;

	// Publish the message
	if(motorEnable){
		m_pwm_publisher.publish(pwm_msg);
	}
	
	
	// m_pwm_publisher_r.publish(pwm_msg_r);


	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	m_timer_for_publishing.stop();
	// > Set the period again (second argument is reset)
	m_timer_for_publishing.setPeriod( ros::Duration(0.02), true);
	// > Start the timer again
	m_timer_for_publishing.start();
	// ROS_INFO_STREAM("[MOTOR CONTROLLER NODE] pwm message sent (" << pwm_l << ","<< pwm_r <<")"<< std::endl);
}

// > Callback run when subscriber receives a message
void thetaDotSubscriberCallback(const ThetaDotMessage& theta_dot){
	// Display that a message was received
    theta_dot_l_ref = theta_dot.left;
    theta_dot_r_ref = theta_dot.right;

	// ROS_INFO_STREAM("[MOTOR CONTROLLER NODE] theta_dot received (" << theta_dot.theta_dot_l << ","<< theta_dot.theta_dot_r<<")"<< std::endl);
}

void encoderSubscriberCallback(const LeftAndRightInt& measured_count){

	// odom should update at the same rate as motion_controller
	float scale_to_per_sec = 10.0;
	
	measured_theta_dot_l = (RPM_TO_RADS*measured_count.left*S_TO_MIN*scale_to_per_sec)/(counts_per_rev);
	measured_theta_dot_r = (RPM_TO_RADS*measured_count.right*S_TO_MIN*scale_to_per_sec)/(counts_per_rev);

    // Display that a message was received
    ROS_INFO_STREAM("[MOTOR CONTROLLER NODE] Message receieved encoder counts: "<< measured_count.left << " , " << measured_count.right);
}

void motorStartStopCommandCallback(const std_msgs::Bool& msg){
	motorEnable = msg.data;
	ROS_INFO_STREAM("[MOTOR CONTROLLER] MotorEnableCommand received with msg: "<<(int)msg.data<<"."<<std::endl);
	if(motorEnable==false){
		ROS_INFO_STREAM("[MOTOR CONTROLLER] MotorEnable False");
		PWMMessage pwm_msg = PWMMessage();
		// std_msgs::Float32 pwm_msg_l;
		pwm_msg.v_r = 0;
		// std_msgs::Float32 pwm_msg_r;
		pwm_msg.v_l = 0;

		// Publish the message
		m_pwm_publisher.publish(pwm_msg);
	} else {
		
		ROS_INFO_STREAM("[MOTOR CONTROLLER] MotorEnable TRUE");
	}
}



//    ------------------------
//    M   M    A    III  N   N
//    MM MM   A A    I   NN  N
//    M M M  A   A   I   N N N
//    M   M  AAAAA   I   N  NN
//    M   M  A   A  III  N   N
//    ------------------------

int main(int argc, char* argv[]){

	// STARTING THE ROS NODE

	ros::init(argc, argv, "motor_controller");

	// CONSTRUCT A NODE HANDLE
	ros::NodeHandle nodeHandle("/");

	// GET THE NAMESPACE OF THIS NODE
	// > This can be useful for debugging because you may be
	//   launching the same node under different namespaces.
	std::string m_namespace = ros::this_node::getNamespace();
	// Display the namespace
	ROS_INFO_STREAM("[MOTOR CONTROLLER	CPP NODE] ros::this_node::getNamespace() =  " << m_namespace);

	// INITIALISE THE PUBLISHER
	// ros::Publisher advertise<<message_type>>(const std::string& topic, uint32_t queue_size, bool latch = false);
	// m_error_message_publisher = nodeHandle.advertise<ThetaDotMessage>("/motor_error_topic", 10, false);
	//   ros::Publisher advertise<<message_type>>(const std::string& topic, uint32_t queue_size, bool latch = false);
    m_pwm_publisher = nodeHandle.advertise<PWMMessage>(m_namespace + "/set_motor_duty_cycle", 10, false);
    // m_pwm_publisher_r = nodeHandle.advertise<std_msgs::Float32>(m_namespace + "/set_motor_duty_cycle_r", 10, false);


	m_timer_for_publishing = nodeHandle.createTimer(ros::Duration(0.02), timerCallbackForPublishing, true);
    ros::Subscriber encoder_subscriber = nodeHandle.subscribe(m_namespace+"/encoder_counts", 1, encoderSubscriberCallback); //Check this is right namespace
	ros::Subscriber theta_dot_subscriber = nodeHandle.subscribe(m_namespace + "/theta_dot_topic", 1, thetaDotSubscriberCallback);
	ros::Subscriber motorStartStopCommandSubscriber = nodeHandle.subscribe("/motorStartStopCommand", 1, motorStartStopCommandCallback);



	// Display that the node is ready
	ROS_INFO("[MOTOR CONTROLLER CPP NODE] Ready :-)");

	// SPIN AS A SINGLE-THREADED NODE
	ros::spin();

	return 0;
}
