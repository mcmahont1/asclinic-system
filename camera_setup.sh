DEVICE=/dev/video0
echo "now adjusting camera settings for $DEVICE"
v4l2-ctl --device=$DEVICE --set-ctrl=focus_auto=0
v4l2-ctl --device=$DEVICE --set-ctrl=focus_absolute=0
v4l2-ctl --device=$DEVICE --set-fmt-video=width=1920,height=1080,pixelformat=MJPG

echo "  "
echo "Camera settings"

v4l2-ctl --device=$DEVICE --get-ctrl=focus_auto,focus_absolute
v4l2-ctl --device=$DEVICE --get-fmt-video



